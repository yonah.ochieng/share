-- public."MantleDC" definition

-- Drop table

-- DROP TABLE public."MantleDC";

CREATE TABLE public."MantleDC" (
	"Child Sku" int8 NOT NULL,
	"Parent Sku" varchar NULL,
	"Mantle SKU" varchar NULL,
	"Product Type" varchar NULL,
	"Product Line" varchar NULL DEFAULT 'Detective Conan'::character varying,
	"Category" varchar NULL,
	"Parent Product Name" varchar NULL,
	"Product Name" varchar NULL,
	"Condition" varchar NULL,
	"Language" varchar NULL,
	"Description" varchar NULL,
	"Sale Price" varchar NULL,
	"Average Cost" varchar NULL,
	"Quantity on Hand" varchar NULL,
	"Reorder Point" varchar NULL,
	"Reserve Quantity" varchar NULL,
	"Picking Bin" varchar NULL,
	"Manufacturer SKU" varchar NULL,
	"SKU" varchar NULL,
	"UPC" varchar NULL,
	"ALU" varchar NULL,
	"ASIN" varchar NULL,
	"Mantle Resources" varchar NULL,
	"Comments" varchar NULL,
	"Release Date" varchar NULL,
	"Weight" varchar NULL,
	"Height" varchar NULL,
	"Length" varchar NULL,
	"Width" varchar NULL,
	"Short Code" varchar NULL,
	"Set Name" varchar NULL,
	"Rarity" varchar NULL,
	"Mana Cost" varchar NULL,
	"Color" varchar NULL,
	"Card Type" varchar NULL,
	"Rules Text" varchar NULL,
	"Power" varchar NULL,
	"Toughness" varchar NULL,
	"Flavor Text" varchar NULL,
	"Artist" varchar NULL,
	"Card Number" varchar NULL,
	"TCG Low Price" varchar NULL,
	"TCG Direct Low" varchar NULL,
	"TCG Market Price" varchar NULL,
	"TCG Price Date" varchar NULL,
	"Buylist Price" varchar NULL,
	"multiverseId" varchar NULL,
	"Image" varchar NULL,
	brand_id int8 NULL,
	set_id int8 NULL,
	rarity_id int4 NULL,
	condition_id int4 NULL,
	date_created timestamptz(0) NULL DEFAULT timezone('utc'::text, now()),
	date_updated timestamptz(0) NULL,
	created_by varchar NULL,
	updated_by varchar NULL,
	CONSTRAINT mantledc_pk PRIMARY KEY ("Child Sku")
);
CREATE INDEX mantledc_brand_id_idx ON public."MantleDC" USING btree (brand_id, set_id, rarity_id, condition_id);
CREATE INDEX mantledc_child_sku_idx ON public."MantleDC" USING btree ("Child Sku");